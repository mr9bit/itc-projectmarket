from rest_framework import routers, serializers, viewsets
from api.models import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'second_name','email',\
            'phone', 'competence', 'is_superuser', 'is_staff', 'role']


class VacancySerializer(serializers.ModelSerializer):
    class Meta:
        model = Vacancy
        fields = ['id', 'title', 'skills_required', 'skills_as_plus', 'project']


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'title', 'description', 'expected_results', 'status',\
            'result_represent_form', 'is_payable', 'payment_amount',\
                'comment', 'partners', 'users']


class CompetenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Competence
        fields = ['id', 'title']
