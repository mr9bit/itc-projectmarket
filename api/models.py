from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager

class Competence(models.Model):
    title = models.CharField(max_length=600, verbose_name="Название", unique=True)

    def __str__(self):
        return self.title

class User(AbstractBaseUser, PermissionsMixin):
    ROLES = [
        (u'partner', u'Партнер'),
        (u'student', u'Студент'),
        (u'moderator', u'Работник IT-Центра'),
        (u'project_lead', u'Лидер проекта')
    ]    
    
    first_name = models.CharField(max_length=400, verbose_name="Имя")
    second_name = models.CharField(max_length=400, verbose_name="Фамилия")
    email = models.EmailField(verbose_name="Email", unique=True)
    phone = models.CharField(max_length=20, verbose_name="Телефон", unique=True)
    competence = models.ManyToManyField("Competence", verbose_name="Компетеции")
    is_superuser = models.BooleanField(default=False, verbose_name="Администратор")
    is_staff = models.BooleanField(default=False)
    role = models.CharField(max_length=600, choices=ROLES, default='student', verbose_name="Роль в проекте")


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return f"{self.first_name} {self.second_name}"
    
    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name


class Project(models.Model):
    STATUSES = [
        (u'uncheked', u'Не проверено'),
        (u'cheked', u'Проверено'),
        (u'filtered', u'Пройден экспертный фильтр'),
        (u'lead_selected', u'Назначен лидер'),
        (u'in_progress', u'Выполняется'),
        (u'archive', u'Архив')
    ]
    title = models.CharField(max_length=600, verbose_name="Название")
    description = models.TextField(verbose_name="Описание")
    expected_results = models.TextField(verbose_name="Ожидаемые результаты")
    status = models.CharField(max_length=600, choices=STATUSES, default='uncheked', verbose_name="Статус")
    result_represent_form = models.TextField(verbose_name="Результат представляет форму")
    is_payable = models.BooleanField(verbose_name="Оплачивается", default=False)
    payment_amount = models.IntegerField(verbose_name="Цена проекта", null=True, blank=True)
    comment = models.TextField(verbose_name="Комментарии", null=True, blank=True)
    partners = models.TextField(verbose_name="Партнеры", null=True, blank=True)
    users = models.ManyToManyField(User, verbose_name="Пользователи проекта")
    
    def __str__(self):
        return self.title


class Vacancy(models.Model):
    title = models.CharField(max_length=600, verbose_name="Название")
    skills_required = models.ManyToManyField(Competence, verbose_name="Необходимые навыки", related_name='competence_req')
    skills_as_plus = models.ManyToManyField(Competence, verbose_name="")
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True)
    
    def __str__(self):
        return self.title

